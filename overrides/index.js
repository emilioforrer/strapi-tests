'use strict';

// var controller = require("@strapi/strapi/lib/core-api/controller/index")
const controllerOverrride = require("./controller.js")
var factories = require('@strapi/strapi').factories
factories.createCoreController = (uid, cfg = {}) => {
  return ({ strapi }) => {
    const baseController = controllerOverrride.createController({
      contentType: strapi.contentType(uid),
    });

    let userCtrl = typeof cfg === 'function' ? cfg({ strapi }) : cfg;

    for (const methodName of Object.keys(baseController)) {
      if (userCtrl[methodName] === undefined) {
        userCtrl[methodName] = baseController[methodName];
      }
    }

    Object.setPrototypeOf(userCtrl, baseController);
    return userCtrl;
  };
}