'use strict';
/**
 * OVERRIDE OF @strapi/strapi/lib/core-api/controller/index
 */
const { getOr } = require('lodash/fp');

const { contentTypes, sanitize } = require('@strapi/utils');

const { transformResponse } = require('@strapi/strapi/lib/core-api/controller/transform');
const createSingleTypeController = require('@strapi/strapi/lib/core-api/controller/single-type');
const createCollectionTypeController = require('./controller/collection-type.js');

const getAuthFromKoaContext = getOr({}, 'state.auth');

const createController = ({ contentType }) => {
  const ctx = { contentType };

  const proto = {
    transformResponse(data, meta) {
      return transformResponse(data, meta, { contentType });
    },

    sanitizeOutput(data, ctx) {
      const auth = getAuthFromKoaContext(ctx);

      return sanitize.contentAPI.output(data, contentType, { auth });
    },

    sanitizeInput(data, ctx) {
      const auth = getAuthFromKoaContext(ctx);

      return sanitize.contentAPI.input(data, contentType, { auth });
    },
  };

  let ctrl;

  if (contentTypes.isSingleType(contentType)) {
    ctrl = createSingleTypeController(ctx);
  } else {
    ctrl = createCollectionTypeController(ctx);
  }

  return Object.assign(Object.create(proto), ctrl);
};

module.exports = { createController };
