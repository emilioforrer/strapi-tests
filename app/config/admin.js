module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '5483efbc101f27994aebc4e7700f9b69'),
  },
});
